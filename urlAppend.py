import logging
from urllib.parse import urlparse

logger = logging.getLogger(__name__)


def url_convert(url):

    if not urlparse(url).scheme:
        url = "http://" + url
    logger.debug(url)
    logger.debug(urlparse(url))


if __name__ == "__main__":
    url_convert("preprod.taskmonk.io")

# print(urlparse("preprod.taskmonk.io"))
